document.addEventListener("DOMContentLoaded", function() {
  // Get the form elements
  var quoteForm = document.getElementById("quoteForm");
  var formFooter = document.getElementById("form-footer");

  // Generate random numbers for the math equation
  var num1 = Math.floor(Math.random() * 10) + 1;
  var num2 = Math.floor(Math.random() * 20) + 1;

  // Set the math equation in the HTML
  var mathEquation = document.getElementById("mathEquation");
  var mathEquation2 = document.getElementById("footermathEquation");
  mathEquation.textContent = num1 + " + " + num2;
  mathEquation2.textContent = num1 + " + " + num2;

  // Function to display error message for an input field
  function displayErrorMessage(inputElement, message) {
    var errorElement = inputElement.nextElementSibling;
    errorElement.textContent = message;
    errorElement.style.display = "block";
  }

  // Validate email
  function validateEmail(email) {
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  //Validate phone number for Canada, USA, and North America
  function validatePhoneNumber(phone) {
    var phoneRegex = /^(\+1[-.\s]?)?(?:\(\d{3}\)|\d{3})[-.\s]?\d{3}[-.\s]?\d{4}$/;
    return phoneRegex.test(phone) && phone.startsWith("+1");
  }
        

  // Validate the math answer
  function validateMathAnswer(mathAnswer) {
    var correctAnswer = num1 + num2;
    return parseInt(mathAnswer) === correctAnswer;
  }

  // Add form submission event listener for the quote form
  quoteForm.addEventListener("submit", function(e) {
    e.preventDefault(); // Prevent the form from submitting normally

    // Reset error messages
    var errorMessages = quoteForm.getElementsByClassName("error-message");
    for (var i = 0; i < errorMessages.length; i++) {
      errorMessages[i].style.display = "none";
    }

    // Validate email
    var emailInput = document.getElementById("email");
    var email = emailInput.value.trim();
    if (!validateEmail(email)) {
      displayErrorMessage(emailInput, "Please enter a valid email address.");
      emailInput.focus();
      return;
    }

    // Validate phone number
    var phoneInput = document.getElementById("phone");
    var phone = phoneInput.value.trim();
    if (!validatePhoneNumber(phone)) {
      displayErrorMessage(phoneInput, "Please enter a valid phone number from Canada, USA, or North America in the format +1 XXX-XXX-XXXX or XXX-XXX-XXXX.");
      phoneInput.focus();
      return;
    }

    // Validate the math answer
    var mathAnswerInput = document.getElementById("mathAnswer");
    var mathAnswer = mathAnswerInput.value.trim();
    if (!validateMathAnswer(mathAnswer)) {
      displayErrorMessage(mathAnswerInput, "Please solve the math equation correctly.");
      mathAnswerInput.focus();
      return;
    }

    // Prepare the form data
    var formData = {
      name: document.getElementById("name").value,
      email: email,
      phone: phone,
      company: document.getElementById("company").value
    };

     //  Check if the "quote-message" field has a value
     var quoteMessage = $("#quote-message").val().trim();
     if (quoteMessage !== "") {
       formData.message = quoteMessage;
     } else {
       formData.message = "No Message";
     }

    // If all validations pass, send the form data via AJAX
   $.ajax({
     url: "submit-form.php", // Sending the data to PHP
     method: "POST",
     dataType: "json",
     data: {
       data: formData,
     },
     success: function(response) {
       console.log(response.status);
       if (response.status === 'success') {
         // Perform any success actions here
         // Scroll to the form heading
          $('html, body').animate({
            scrollTop: $("#formHeading").offset().top - 120
          }, 500);

         quoteForm.reset();
         $("#quoteForm").hide(); // hiding the form
         $("#formHeading").focus;
         $("#form-area").addClass("loader-bg");
         $(".success-loader").show(); // showing loader

         setTimeout(function() {
           $(".success-loader").hide(); // Hide the loader
           $("#form-area").removeClass("loader-bg");
           $("#form-area .success-message").fadeIn(); // Showing the success message block
           $("#formHeading").text("Thank you!").addClass("display-3");
         }, 3000);
         
       } else {
         // Handle the error case
         var errorFields = response.errors;
         for (var field in errorFields) {
           if (errorFields.hasOwnProperty(field)) {
             var inputElement = document.getElementById(field);
             var errorMessage = errorFields[field];
             displayErrorMessage(inputElement, errorMessage);
           }
         }
       }
     },
     error: function() {
       console.log('error');
       // Handle the error case
       alert("An error occurred. Please try again later.");
     }
   });

   e.preventDefault(); // Prevent the form from submitting normally
  });

  // Add form submission event listener for the footer form
  formFooter.addEventListener("submit", function(e) {
    e.preventDefault(); // Prevent the form from submitting normally

    // Reset error messages
    var errorMessages = formFooter.getElementsByClassName("error-message");
    for (var i = 0; i < errorMessages.length; i++) {
      errorMessages[i].style.display = "none";
    }

    // Validate email
    var emailInput = document.getElementById("email-footer");
    var email = emailInput.value.trim();
    if (!validateEmail(email)) {
      displayErrorMessage(emailInput, "Please enter a valid email address.");
      emailInput.focus();
      return;
    }

    // Validate phone number
    var phoneInput = document.getElementById("phone-footer");
    var phone = phoneInput.value.trim();
    if (!validatePhoneNumber(phone)) {
      displayErrorMessage(phoneInput, "Please enter a valid phone number from Canada, USA, or North America in the format +1 XXX-XXX-XXXX");
      phoneInput.focus();
      return;
    }

    // Validate the math answer
    var mathAnswerInput = document.getElementById("mathAnswer-footer");
    var mathAnswer = mathAnswerInput.value.trim();
    if (!validateMathAnswer(mathAnswer)) {
      displayErrorMessage(mathAnswerInput, "Please solve the math equation correctly.");
      mathAnswerInput.focus();
      return;
    }

    // Prepare the form data
    var formData = {
      name: document.getElementById("name-footer").value,
      email: email,
      phone: phone,
      company: document.getElementById("company-footer").value,
      message: "" // Blank message
    };
// If all validations pass, send the form data via AJAX
$.ajax({
  url: "submit-form.php", // Sending the data to PHP
  method: "POST",
  dataType: "json",
  data: {
    data: formData,
  },
  success: function(response) {
    console.log(response.status);
    if (response.status === 'success') {
      // Perform any success actions here (e.g., reset form, show success message)
      quoteForm.reset();
      $("#form-footer").hide(); // hiding the form
      $("#form-footer-area").addClass("loader-bg");
      $(".footer .success-loader").show(); // showing loader

      setTimeout(function() {
        $(".footer .success-loader").hide(); // Hide the loader
        $("#form-footer-area").removeClass("loader-bg");
        $("#form-footer-area").addClass("success-bg");
        $(".footer .success-message").fadeIn(); // Showing the success message block
        $(".footer #footer-heading").text("Thank you!").addClass("display-3");
      }, 3000);
      
    } else {
      // Handle the error case
      var errorFields = response.errors;
      for (var field in errorFields) {
        if (errorFields.hasOwnProperty(field)) {
          var inputElement = document.getElementById(field);
          var errorMessage = errorFields[field];
          displayErrorMessage(inputElement, errorMessage);
        }
      }
    }
  },
  error: function() {
    console.log('error');
    // Handle the error case
    alert("An error occurred. Please try again later.");
  }
});

e.preventDefault(); // Prevent the form from submitting normally
 
  });
});

