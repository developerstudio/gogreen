'use strict';

(function ($) {

    /*------------------------------------------------------------------------------*/
    /* Fixed-header
    /*------------------------------------------------------------------------------*/

    $(window).scroll(function(){
        if ( matchMedia( 'only screen and (min-width: 1200px)' ).matches ) 
        {
            if ($(window).scrollTop() >= 50 ) {

                $('.header').addClass('fixed-header');
                $('.header_top').css('display', 'none');
            }
            else {

                $('.header').removeClass('fixed-header');
                $('.header_top').css('display', 'block');
            }
        }
        else if (matchMedia( 'only screen and (max-width: 767px)').matches)
        {
            if ($(window).scrollTop() >= 50 ) {

                $('.canvas_open').addClass('fixed_hamburger');
            }
            else {

                $('.canvas_open').removeClass('fixed_hamburger');
            }
        }
    });

    // Function to handle active class for navigation links
    function handleActiveClass() {
        var navLinks = $('.header__menu a');

        navLinks.on('click', function() {
            // Remove active class from the parent <li> elements
            $(this).closest('li').siblings().removeClass('active');
            // Add active class to the parent <li> element
            $(this).closest('li').addClass('active');
        });
    }

    $(function() {
        $('a[href^="#"]').on('click', function(e) {
            if (this.hash !== '') {
                e.preventDefault();

                var hash = this.hash;
                var nav = $('.header').outerHeight();
                var overshoot = 5 * parseFloat($('body').css('font-size')); // Calculate the overshoot in pixels (3em in this case)

                $('html, body').animate({
                    scrollTop: $(hash).offset().top - nav - overshoot
                }, 2000);
            }
        });

        handleActiveClass();
    });    

    /*------------------------------------------------------------------------------*/
    /* Back to top
    /*------------------------------------------------------------------------------*/
    
    // ===== Scroll to Top ==== 
    $('#totop').hide();

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 1000) {        // If page is scrolled more than 50px
            $('#totop').fadeIn(200);    // Fade in the arrow
            $('#totop').addClass('top-visible');
        } else {
            $('#totop').fadeOut(200);   // Else fade out the arrow
            $('#totop').removeClass('top-visible');
        }
    });

    $('#totop').on("click",function() {      // When arrow is clicked
        
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 500);
        return false;
    });


    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function () {
        setTimeout(function() {
            $("#preloader").fadeOut("slow");
        }, 8000); // Delay the fade out of the preloader for 10 seconds
    });            

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    //Canvas Menu
    $(".canvas_open").on('click', function () {
        $(".offcanvas-menu-wrapper").addClass("active");
        $(".offcanvas-menu-overlay").addClass("active");
    });

    $(".offcanvas-menu-overlay").on('click', function () {
        $(".offcanvas-menu-wrapper").removeClass("active");
        $(".offcanvas-menu-overlay").removeClass("active");
    });
    $(".offcanvas-menu-wrapper").on('click', function () {
        $(".offcanvas-menu-wrapper").removeClass("active");
        $(".offcanvas-menu-overlay").removeClass("active");
    });

    //Search Switch
    $('.search-switch').on('click', function () {
        $('.search-model').fadeIn(400);
    });

    $('.search-close-switch').on('click', function () {
        $('.search-model').fadeOut(400, function () {
            $('#search-input').val('');
        });
    });

    /*------------------
        Navigation
    --------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true,
        showParent: true
    });

    /*-----------------------
        Partner Slider
    ------------------------*/
    $(".partner__logo__slider").owlCarousel({
        loop: true,
        margin: 140,
        items: 6,
        dots: false,
        nav: false,
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        mouseDrag: false,
        responsive: {
            320: {
                items: 2,
                margin: 50
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            },
            1450: {
                items: 6
            }
        }
    });

    /*-----------------------
        Projects Slider
    ------------------------*/
    $(".projects__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 3,
        dots: true,
        nav: false,
        smartSpeed: 1200,
        autoHeight: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        responsive: {
            320: {
                items: 1
            },
             768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    /*-----------------------
        Testimonial Slider
    ------------------------*/
    $(".testimonial__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        dots: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'><i/>", "<i class='fa fa-angle-right'><i/>"],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true
    });

    /*--------------------------
        Select
    ----------------------------*/
    $("select").niceSelect();

    /*------------------
        Achieve Counter
    --------------------*/
    $('.c_num').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

})(jQuery);
