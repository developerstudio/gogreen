<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Retrieve the form data
  $formData = $_POST['data'];

  // Extract individual form fields
  $name = $formData['name'];
  $email = $formData['email'];
  $phone = $formData['phone'];
  $company = $formData['company'];
  $message = $formData['message'];

  // Compose the email subject and body
  $subject = "Form Submission from " . $name;
  $body = "Name: " . $name . "\n";
  $body .= "Email: " . $email . "\n";
  $body .= "Phone: " . $phone . "\n";
  $body .= "Company: " . $company . "\n";
  $body .= "Message: " . $message . "\n";

  // Set the recipient email address
  $to = "info@gogreentransport.ca";

  // Set additional headers
  $headers = "From: " . $email . "\r\n";
  $headers .= "Reply-To: " . $email . "\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html\r\n";

  // Send the email
  $mailSent = mail($to, $subject, $body, $headers);

  // Check if the email was sent successfully
  // Check if the email was sent successfully
  if ($mailSent) {
    // Return a success response
    http_response_code(200);
    echo json_encode(['status' => 'success']);
  } else {
    // Return an error response
    http_response_code(500);
    echo json_encode(['status' => 'error']);
  }
} else {
  // Return an error response
  http_response_code(400);
  echo json_encode(['status' => 'invalid_request']);

}
?>
